//
//  NeUIButton.swift
//  NeumorphismKit
//
//  Created by Fernando Martin Garcia Del Angel on 01/07/20.
//

import UIKit
import Lottie

@available(iOS 11.0, *)
@IBDesignable
open class NeUIButton: UIButton {
    // MARK: - Properties
    private var cornerRadius: CGFloat = NeumorphicDefaults.cornerRadius
    private var clrBackground: UIColor = NeumorphicDefaults.backgroundColor
    
    // MARK: - Lottie Animations
    public private(set) var animationView: AnimationView = AnimationView()
    @IBInspectable
    var typeOfAnimation : Int = 0 {
        didSet {
            
        }
    }
    
    // MARK: - UIButton Overridable Methods
    override open var isHighlighted: Bool {
        didSet {
            if isHighlighted {
                setState()
            } else {
                resetState()
            }
        }
    }
    
    
    override open var isEnabled: Bool {
        didSet {
            if isEnabled {
                resetState()
            } else {
                setState()
            }
        }
    }
    
    private func setState() {
        self.layer.shadowOffset = CGSize(width: -2, height: -2)
        self.layer.sublayers?[0].shadowOffset = CGSize(width: 2, height: 2)
        self.contentEdgeInsets = UIEdgeInsets(top: 2, left: 2, bottom: 0, right: 0)
        animationView.play()
    }
    
    private func resetState() {
        self.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.layer.sublayers?[0].shadowOffset = CGSize(width: -2, height: -2)
        self.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 2, right: 2)
    }
    
    // MARK: - Initializers
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        self.makeNeumorphic(cornerRadius: cornerRadius, backgroundColor: clrBackground)
    }
    
    open func addAnimation() {
        let testView = UIView(frame:self.frame)
        testView.backgroundColor = .red
        self.addSubview(testView)
    }
    
    required public init(cornerRadius: CGFloat = 15.0, backgroundColor: UIColor = .background) {
        self.cornerRadius = cornerRadius
        self.clrBackground = backgroundColor
        super.init(frame: .zero)
    }
    
    open override func draw(_ rect: CGRect) {
        super.draw(rect)
        let animationPack : ButtonAnimationLayout = getAnimation(selected: NeumorphicButtonType(rawValue: typeOfAnimation) ?? NeumorphicButtonType.heart)
        animationView = AnimationView(animation: animationPack.getAnimation(isSelected: true))
        animationView.frame = rect
        animationView.isUserInteractionEnabled = false
        self.addSubview(animationView)
    }
    
    private func getAnimation(selected: NeumorphicButtonType) -> ButtonAnimationLayout {
        switch selected {
        case .heart:
            return HeartAnimations()
        case .share:
            return ShareAnimations()
        case .none:
            return EmptyAnimation()
        }
    }

    // MARK: - Dark Mode Methods
    open override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        if #available(iOS 13.0, *) {
            if traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) {
                self.updateColor(backgroundColor: clrBackground)
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
}
