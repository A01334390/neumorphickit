//
//  NeuUIView.swift
//  NeumorphismKit
//
//  Created by Fernando Martin Garcia Del Angel on 01/07/20.
//

import UIKit

@available(iOS 11.0, *)
open class NeUIView: UIView {
    
    // MARK: - Properties
    private var cornerRadius: CGFloat = NeumorphicDefaults.cornerRadius
    private var clrBackground: UIColor = NeumorphicDefaults.backgroundColor
    
    // MARK: - Initializers
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        self.makeNeumorphic()
    }
    
    required public init(cornerRadius: CGFloat = 15.0, backgroundColor: UIColor = .background) {
        self.cornerRadius = cornerRadius
        self.clrBackground = backgroundColor
        super.init(frame: .zero)
    }
    
    // MARK: - Dark Mode Methods
    open override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        if #available(iOS 13.0, *) {
            if traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) {
                self.updateColor(backgroundColor: clrBackground)
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
}
