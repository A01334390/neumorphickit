//
//  NeumorphicDefaults.swift
//  NeumorphismKit
//
//  Created by Fernando Martin Garcia Del Angel on 01/07/20.
//

import UIKit
import Lottie
/**
 Default values for the Neumorphic Design Pattern
 */
@available(iOS 11.0, *)
public struct NeumorphicDefaults {
    static let cornerRadius : CGFloat = 15.0
    static let backgroundColor : UIColor = .background
}

@available(iOS 11.0, *)
public enum NeumorphicButtonType : Int {
    case none = 0
    case heart = 1
    case share = 2
}

@available(iOS 11.0, *)
public protocol ButtonAnimationLayout {
    var activeButton : Animation? { get }
    var inactiveButton: Animation? { get }
    func getAnimation(isSelected: Bool) -> Animation?
}

@available(iOS 11.0, *)
public class HeartAnimations : ButtonAnimationLayout {
     public let inactiveButton: Animation? = Animation.named("7500-heart")!
     public let activeButton: Animation? = Animation.named("7500-heart")!
    
    public func getAnimation(isSelected: Bool = true) -> Animation? {
        isSelected ? activeButton : inactiveButton
    }
}

@available(iOS 11.0, *)
public class ShareAnimations : ButtonAnimationLayout {
    public let inactiveButton: Animation? = Animation.named("7167-share-animated-icon")!
    public let activeButton: Animation? = Animation.named("7167-share-animated-icon")!
    
    public func getAnimation(isSelected: Bool = true) -> Animation? {
        isSelected ? activeButton : inactiveButton
    }
}

@available(iOS 11.0, *)
public class EmptyAnimation : ButtonAnimationLayout {
    public let inactiveButton: Animation? = nil
    public let activeButton: Animation? = nil
    
    public func getAnimation(isSelected: Bool = true) -> Animation? {
        nil
    }
}
