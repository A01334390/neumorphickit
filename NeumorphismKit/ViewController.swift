//
//  ViewController.swift
//  NeumorphismKit
//
//  Created by Fernando Martin Garcia Del Angel on 26/06/20.
//

import UIKit
import Lottie

class ViewController: UIViewController {

    @IBOutlet weak var cardView: NeUIView!
    @IBOutlet weak var mexicoButton: NeUIButton!
    @IBOutlet weak var loveButton: NeUIButton!
    @IBOutlet weak var shareButton: NeUIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
}

