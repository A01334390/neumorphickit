//
//  Neumorphism+UIView.swift
//  NeumorphismKit
//
//  Created by Fernando Martin Garcia Del Angel on 01/07/20.
//

import UIKit

@available(iOS 11.0, *)
extension UIView {
    
    /**
     Converts a simple UIView conformed element to have a Neumorphic Design
     - Parameters:
        - cornerRadius: The View corner radius defined as a CGFloat
        - backgroundColor: The application background color as an UIColor
     */
    @objc public func makeNeumorphic(cornerRadius: CGFloat = 15.0, backgroundColor: UIColor = .background) {
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.layer.shadowColor = UIColor.darkShadow.cgColor
        
        let shadowLayer = CAShapeLayer()
        shadowLayer.frame = bounds
        shadowLayer.backgroundColor = backgroundColor.cgColor
        shadowLayer.shadowColor = UIColor.lightShadow.cgColor
        shadowLayer.cornerRadius = cornerRadius
        shadowLayer.shadowOffset = CGSize(width: -2, height: -2)
        shadowLayer.shadowOpacity = 1
        shadowLayer.shadowRadius = 2
        self.layer.insertSublayer(shadowLayer, at: 0)
    }
    
    /**
     Updates the UIView colors to match a Light, Dark or undefined color.
     - Parameter backgroundColor: The application background color as an UIColor
     */
    @objc func updateColor(backgroundColor: UIColor = .background) {
        if let sublayers = self.layer.sublayers {
            for layer in sublayers {
                if layer.isKind(of: CAShapeLayer.self) {
                    layer.shadowColor = UIColor.lightShadow.cgColor
                    layer.backgroundColor = backgroundColor.cgColor
                }
            }
        }
        self.layer.shadowColor = UIColor.darkShadow.cgColor
    }
}
