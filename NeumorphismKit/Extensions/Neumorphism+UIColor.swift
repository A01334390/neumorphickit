//
//  Neumorphism+UIColor.swift
//  NeumorphismKit
//
//  Created by Fernando Martin Garcia Del Angel on 01/07/20.
//

import UIKit

@available(iOS 11.0, *)
extension UIColor {
    /**
     App's Theme Color as defined in the Assets Folder
     */
    public static let themeColor = UIColor(named: "adn40")!
    /**
     App's Global Background color as defined in the Assets Folder
     */
    public static let background = UIColor(named: "background")!
    /**
     App's Dark Shadow color as defined in the Assets Folder
     */
    public static let darkShadow = UIColor(named: "darkShadow")!
    /**
     App's Light Shadow Color as defined in the Assets Folder
     */
    public static let lightShadow = UIColor(named: "lightShadow")!
}
