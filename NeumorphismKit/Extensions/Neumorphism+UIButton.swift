//
//  Neumorphism+UIButton.swift
//  NeumorphismKit
//
//  Created by Fernando Martin Garcia Del Angel on 01/07/20.
//

import UIKit

@available(iOS 11.0, *)
extension UIButton {
    
    public override func makeNeumorphic(cornerRadius: CGFloat = 15.0, backgroundColor: UIColor = .background) {
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.layer.shadowColor = UIColor.darkShadow.cgColor
        
        let shadowLayer = CAShapeLayer()
        shadowLayer.frame = bounds
        shadowLayer.backgroundColor = backgroundColor.cgColor
        shadowLayer.shadowColor = UIColor.lightShadow.cgColor
        shadowLayer.cornerRadius = cornerRadius
        shadowLayer.shadowOffset = CGSize(width: -2.0, height: -2.0)
        shadowLayer.shadowOpacity = 1
        shadowLayer.shadowRadius = 2
        self.layer.insertSublayer(shadowLayer, below: self.imageView?.layer)
    }
}
